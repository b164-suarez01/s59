import {Navigate} from 'react-router-dom';
//^This is the Redirect component
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext';

export default function Logout() {
	//Pra pag naglogout madedelete ung user sa localStorage
	
	const {unsetUser, setUser} = useContext(UserContext);

	//clear the localStorage
	unsetUser()
	
	//By adding the useeffect, this will allo the logout page to render first before triggering the useEffect which changes the state of our user
		useEffect(() => {
			setUser({id:null})
		},[])

	return(
		<Navigate to="/" />
		)

}