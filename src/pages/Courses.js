import {Fragment,useEffect, useState} from 'react';
import CourseCard from '../components/CourseCard';
//import coursesData from '../data/coursesData';

export default function Courses(){

	const [courses,setCourses] = useState([])
	//console.log(coursesData[0])
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} courseProp={course} />
	// 		)
	// })
	//Lalabas lahat ng active courses
	useEffect(() => {
		fetch('http://localhost:4000/api/courses/')
		.then(res => res.json())
		.then(data => {
			console.log(data)
			//need i-map kasi ung data ay array of courses
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />
					)
			}))
		})
	}, [])
	return (
		<Fragment>
			<h1>Course</h1>
			 {courses}
		</Fragment>
		)
}