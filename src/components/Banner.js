import {Col, Row} from 'react-bootstrap';
//New Task: Modify the banner component so that it would be able to accept data from its parent's components
//Props is synonymous to fxn arguments/parameters, because arguments/parameters => catch and get data input
//we are going to define the prop in this component as an object
//place the properties of each object to its intended section in the component
export default function Banner({bannerData}){
	return(
		<Row>
			<Col className="p-5 m-3">
				<h1 className="text-center text-success"> {bannerData.title}</h1>
				<p className="text-center my-4">{bannerData.content} </p>
			</Col>	
		</Row>
		);
};
