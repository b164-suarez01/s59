import {Card as Baraha} from 'react-bootstrap';
export default function CourseCard(){
	return(
		<Baraha>
			<Baraha.Body>
				<Baraha.Title>
					INSERT COURSE NAME HERE
				</Baraha.Title>
				<Baraha.Text>
					Price: INSERT COURSE PRICE
				</Baraha.Text>	
				<Baraha.Text>
					Description: INSERT COURSE DESCRIPTION
				</Baraha.Text>
			</Baraha.Body>
		</Baraha>
		);
}