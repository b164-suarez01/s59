import {Navbar, Nav} from 'react-bootstrap';

//[SECTION] New task: Modify the NavBar to make it a functional component.
//1. Acquire the Link utility from react-router-dom
import {Link} from 'react-router-dom';
	//Link => allow us to redirect a user to a specific path/location
	//the link component has only one attribute:
		//to => used to assign a designated location/path to redirect the user
	//Task: Modify the appearance of the Link component, so that it will blend accordingly to the skin of the navbat instead of looking like an actual anchor tag
		//sol: use utility class from bootstrap

export default function AppNavbar(){
	return(
		<Navbar bg="light" expand="lg">
			<Navbar.Brand>
				E-Booking Engineering Courses
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Link className="nav-link" to='/'>Home</Link>
					<Link className="nav-link" to='/register'>Register</Link>
					<Link className="nav-link" to='/login'>Login</Link>
					<Link className="nav-link" to='/courses'>Courses</Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
		)
};
