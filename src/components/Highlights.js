import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';


//lets use the css module of react to add skin to our component
export default function Highlights() {
	return(
		<Row className="mt-5 mb-3">
			<Col xs={12} md={4}>
				{/*1st Card Component*/}
				<Card className ="p-3 cardHighlight" >
					<Card.Body>
						<Card.Title>Learn from Home</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati, praesentium! Saepe, provident dolor exercitationem corrupti!
						</Card.Text>
					</Card.Body>	
				</Card >
			</Col>
			<Col xs={12} md={4}>
				{/*2nd Card Component*/}
				<Card className ="p-3 cardHighlight">
					<Card.Body>
						<Card.Title>Study Now, Pay Later</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati, praesentium! Saepe, provident dolor exercitationem corrupti!
						</Card.Text>
					</Card.Body>	
				</Card >
			</Col>
			<Col xs={12} md={4}>
				{/*3rd Card Component*/}
				<Card className ="p-3 cardHighlight">
					<Card.Body>
						<Card.Title>Be Part of Our Community</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati, praesentium! Saepe, provident dolor exercitationem corrupti!
						</Card.Text>
					</Card.Body>	
				</Card>
			</Col>
		</Row>		
		);
};
